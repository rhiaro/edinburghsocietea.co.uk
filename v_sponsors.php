<h3>We're grateful for support from</h3>
<p>
    <a href="http://eusa.ed.ac.uk" class="inner"><img src="img/s_eusa.png" alt="Edinburgh University Students' Associatoin (EUSA)" /></a>
    <a href="http://anteaques.co.uk" class="inner"><img src="img/s_anteaques.png" alt="Anteaques" /></a>
    <a href="http://eteaket.co.uk" class="inner"><img src="img/s_eteaket.png" alt="Eteaket" /></a>
</p>
<p>
    <a href="http://pekoetea.co.uk" class="inner"><img src="img/s_pekoe.png" alt="Pekoe Tea" /></a>
    <a href="http://www.loopylornas.com/" class="inner"><img src="img/s_lornas.png" alt="Loopy Lorna's" /></a>
    <span class="inner"><a href="http://www.betterbeverage.co.uk/">The Better Beverage Company</a></span>
</p>