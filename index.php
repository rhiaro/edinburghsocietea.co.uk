<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>University of Edinburgh SocieTea</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body class="color2-texture">
        <!--[if lt IE 7]>
            <p class="fail">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

            <div class="wrapper clearfix">
                <header class="w1of4 align-center">
                    <h1><a href="index.php"><img src="img/crest.png" alt="The University of Edinburgh SocieTea"></a></h1>
                    <nav>
                        <img src="img/linetop.png" alt="" role="presentation" />
                        <ul class="inner">
                            <li><a href="http://edinburghsocietea.blogspot.com" class="deco-border">Teablog</a></li>
                        </ul>
                        <img src="img/linebot.png" alt="" role="presentation" />
                    </nav>
                    
                </header>
                <div class="w3of4">
                    <div class="w1of2"><div class="inner">
                        <h2>Hey, tea lover</h2>
                        <p>At the University of Edinburgh Tea Society, civilised gatherings are our speciality.  Whether you're a tea connoisseur or just like a good cuppa, you're welcome to join us for a drink and casual chat.</p>
                        <p>We enjoy high quality teas and infusions from around the world, provided locally by Edinburgh tea shops as well as gathered by our members on their travels.  We try to impart knowledge about their origins and how they were produced as much as possible.  If you choose to join our ranks, you'll have access to our extensive 'library' of teas free of charge.</p>
                        <div id="mc_embed_signup">

                            <p>To stay up to date, join our mailing list.</p>
                            <!-- Begin MailChimp Signup Form -->
                            <div id="mc_embed_signup">
                            <form action="http://edinburghsocietea.us6.list-manage1.com/subscribe/post?u=4c7bc380ebf108b6d1a9b0e38&amp;id=ef03a86a51" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="mc-field-group">
                                <label for="mce-EMAIL">Email Address </label>
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                            </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>  <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                            </form>
                            </div>

                            <!--End mc_embed_signup-->

                        </div>

                        <h3>Don't like tea?</h3>
                        <p>You can't have tried them all.  We drink black, green, white, yellow and flavoured tea, rooibos and herbal infusions.  Challenge us to find something you like.</p>
                        <h3>Membership</h3>
                        <p>Membership is just &pound;5 for the whole academic year, although you're never obliged to join.  Members will be issued with cards which give a discount at a number of fantastic Edinburgh tea shops though, as well as free or discounted access to our special events.  After October, Weekly MeeTeaings are &pound;1 per week for non-members.</p>
                        <p>You can pay for membership in person at any of our events, or <a href="http://www.eusa.ed.ac.uk/societies/society/SocieTea/">on the EUSA website</a>.</p>
                        <h3>CommitTea 2014-15</h3>
                        <p>You can contact us at edinburghSocieTea {at} gmail {dot} com.</p>
                        <ul>
                            <li>El PresidenTea: Matúš Falis <em>(3rd year, Informatics)</em></li>
                            <li>El Vice PresidenTea: Matthew Summers (3rd year, Informatics)</em></li>
                            <li>Tearesurer: Rikki Guy <em>(3rd year, Informatics)</em></li>
                            <li>SecreTeary: Charlotte Lilley <em>(4th year, Biology)</em></li>
                            <li>(Extra)Ordinary member: Amy Guy <em>(PhD, Informatics)</em></li>
                        </ul>
                        <h3>ConsTeatution 2013-14</h3>
                        <p>You can <a href="https://docs.google.com/document/d/1haa-0r6Mkg5OBc8QS9jaxKlKK7y-ms8a3Z0MsuPSg_k/edit?usp=sharing">read our ConsTeatution</a>, as required by EUSA.</p>
                        
                    </div></div>
                    <div class="w1of2"><div class="inner events-holder wee">
                        <? include 'v_events.php'; ?>
                    </div></div>
                </div>
            </div>

            <div class="wrapper clearfix">
                <div class="w1of4"><p></p></div>
                <div class="w3of4"><div class="inner">
                    <? include 'v_sponsors.php'; ?>
                </div></div>
            </div>

            <footer class="w1of1 clearfix dark-bg trans">
                <p></p>
            </footer>

    </body>
</html>
