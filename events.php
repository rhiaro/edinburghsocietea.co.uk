<!-- Add new events to this array, according to the obvious pattern... -->
<?
$events = array(
				array(
					 'title' => "Society Variety"
					,'date' => strtotime("1200 8th September 2013")
					,'price' => "FREE"
					,'place' => "Potterrow Dome"
					,'map' => "#"
					,'descr' => "Find us at the Societies' Fare for international students."
				)	
				,array(
					 'title' => "Tea Tasting Extravaganza"
					,'date' => strtotime("1800 9th September 2013")
					,'price' => "FREE"
					,'place' => "Teviot Dining Room"
					,'map' => "#"
					,'descr' => "Join us to sample a selection of fine teas from around the world, with complementary cakes and biscuits, and prizes to be won!"
				)
				,array(
					 'title' => "Anteaques Guided Tasting"
					,'date' => strtotime("1930 13th September 2013")
					,'price' => "FREE"
					,'place' => "Anteaques, 17 Clerk Street, Edinburgh"
					,'map' => "#"
					,'descr' => "Try a variety of high quality teas and learn about them from the experts at the incredible Anteaques tea shop.  Places are limited, so please <a href=\"http://".$_SERVER['HTTP_HOST']."/signups\">sign up here!</a>"
				)
				,array(
					 'title' => "Welcome (Weekly MeeTeaing)"
					,'date' => strtotime("1730 19th September 2013")
					,'price' => "FREE"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Come along and introduce yourself! Everyone is welcome, for board games, civilised chitchat and, of course, copious amounts of tea."
				)	
				,array(
					 'title' => "T-ea-ravels (Weekly MeeTeaing)"
					,'date' => strtotime("1730 26th September 2013")
					,'price' => "FREE"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Sample teas that our members have brought from around the world - and please bring your own if you have some!"
				)
				,array(
					 'title' => "The LocaliTea (Weekly MeeTeaing)"
					,'date' => strtotime("1730 3rd October 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Try a tea from each of some of the local tea shops and experts who have supported us over the years."
				)
				,array(
					 'title' => "Weekly MeeTeaing"
					,'date' => strtotime("1730 10th October 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Enjoy teas of your choice from our vast supplies, and if you like, learn to play Barons of Tea."
				)
				,array(
					 'title' => "Tea &amp; Massage"
					,'date' => strtotime("1730 17th October 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "We're TEAming up with ChillSoc for the ultimate evening of relaxation. Get a professional head or shoulder massage (with oil and everything) whilst you enjoy your cuppa."
				)
				,array(
					 'title' => "Weekly MeeTeaing"
					,'date' => strtotime("1730 24th October 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Enjoy teas recently brought back from Japan by some of our members."
				)
				,array(
					 'title' => "Tasseography"
					,'date' => strtotime("1730 31st October 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "In the spirit of Hallowe'en, peer at your tea leaves to learn your fate and fortune.  OooOOOooo.  Fancy dress optional."
				)
				,array(
					 'title' => "ImiTEAtions (an informal tasting)"
					,'date' => strtotime("1730 7th November 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Guided by James of Edinburgh-based <a href=\"http://driftwoodtea.com/\">Driftwood Tea</a>, we'll compare some famous Taiwanese teas with their fake Chinese counterparts. You'll also be able to buy some of James's teas at a considerable discount!"
				)
				,array(
					 'title' => "Tea and Marketing: a tasting with The Better Beverage Company"
					,'date' => strtotime("1730 5th December 2013")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Try some interesting samples and hear about tea and marketing over the years with Dave of the <a href=\"http://www.betterbeverage.co.uk/\">Better Beverage Company</a>."
				)
				,array(
					 'title' => "Pottery PainTEAing workshop"
					,'date' => strtotime("1700 12th December 2013")
					,'price' => "£3 / ~£10-15 non-members"
					,'place' => "Doodles, Marchmont Cresent"
					,'map' => "#"
					,'descr' => "Create your own beautiful cup and saucer set (or upgrade to a teapot!) at Doodles Pottery Painting studio. <a href=\"https://docs.google.com/forms/d/1sBbsYb_uCHbNJ_MuwXwW1ieiwD8AZikkzzog0T2y1Ps/viewform\">Sign up here</a>."
				)
				,array(
					 'title' => "Weekly MeeTEAing"
					,'date' => strtotime("1730 30th January 2014")
					,'price' => "FREE"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Chill out, sip your choice of teas from our extensive library, and play board games if you fancy."
					)
				,array(
					 'title' => "Weekly MeeTEAing"
					,'date' => strtotime("1730 6th February 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Chill out, sip your choice of teas from our extensive library, and play board games if you fancy."
					)
				,array(
					 'title' => "Weekly MeTEAing"
					,'date' => strtotime("1730 13th February 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "We're joined by Jon from Pekoe Tea who will guide us through some delicious beverages."
					)
				,array(
					 'title' => "InnovaTEAve Learning Week (blend-your-own)"
					,'date' => strtotime("1730 20th February 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Mix and match teas, herbs and spices to create your very own tea blend. Please do bring your own ingredients to share!"
					)
				,array(
					 'title' => "Tasting with Jon from Pekoe Tea"
					,'date' => strtotime("1730 27th February 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Chill out, sip your choice of teas from our extensive library, and play board games if you fancy."
					)
				,array(
					 'title' => "Monsoon Mountains Tasting"
					,'date' => strtotime("1730 6th March 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Learn about the history of Ceylon and try some amazing teas from Sri Lanka, guided by the <a href=\"https://www.facebook.com/pages/The-Monsoon-Mountains-Tea-Company-Specialsts-in-Ceylon-Teas/222481297766887\">Monsoon Mountains Tea Company</a>."
					)
				,array(
					 'title' => "Weekly MeeTEAing"
					,'date' => strtotime("1730 13th March 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Chill out, sip your choice of teas from our extensive library, and play board games if you fancy."
					)
				,array(
					 'title' => "Darjeeling Tasting with Andrew from Anteaques"
					,'date' => strtotime("1730 20th March 2014")
					,'price' => "FREE / £1 non-members"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Andrew is bringing along loads of Darjeeling samples for us to taste, possibly even some fresh from India."
				)
				,array(
					 'title' => "Annual General Meeting"
					,'date' => strtotime("1730 27th March 2014")
					,'price' => "FREE (members only)"
					,'place' => "Chaplaincy Room 2"
					,'map' => "#"
					,'descr' => "Vote for your new CommitTea! (And run, if you fancy it. <a href=\"agm\">Find out more</a>)."
				)
);
?>