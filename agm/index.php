<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>University of Edinburgh SocieTea</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="../css/normalize.min.css">
        <link rel="stylesheet" href="../css/main.css">

    </head>
    <body class="color2-texture">
        <!--[if lt IE 7]>
            <p class="fail">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

            <div class="wrapper clearfix">
                <header class="w1of4 align-center">
                    <h1><a href="../index.php"><img src="../img/crest.png" alt="The University of Edinburgh SocieTea"></a></h1>
                    <nav>
                        <img src="../img/linetop.png" alt="" role="presentation" />
                        <ul class="inner">
                            <li><a href="http://edinburghsocietea.blogspot.com" class="deco-border">Teablog</a></li>
                        </ul>
                        <img src="../img/linebot.png" alt="" role="presentation" />
                    </nav>
                    
                </header>
                <div class="w3of4">
                    <div class="w1of2"><div class="inner">
                        <h2>Tea needs you</h2>
                        <p>It's AGM time! (27th March 2014).</p>
                        <p>SocieTea is pretty low maintenance. Anyone could run it! Even you...</p>
                        <h3>What does it need?</h3>
                        <p>These are the things that need to happen to keep SocieTea functioning as it does, and who took care of them this year. Your new commitTea is welcome to mix and match duties and roles however you please.</p>
                        <ul>
                            <li>Sending weekly emails about meetings and events (President)</li>
                            <li>Setting up the room for meetings every week (VP, SecreTeary, Ordinary Member, anyone else available)</li>
                            <li>Emailing local tea shops/suppliers to arrange special events, tastings etc (President)</li>
                            <li>Keeping on top of jumping through EUSA's hoops, filling in forms etc. (President, SecreTeary)</li>
                            <li>Book-keeping and finances (writing down what money we spend and what money we get - Teareasurer)</li>
                            <li>Running meetings, making tea, etc. (Take it in turns / President delegates)</li>
                            <li>Cleaning up after meetings (everyone)</li>
                            <li>Taking responsiblity, for comic effect, for things breaking, hot water spilled, any and all minor accidents (Secretary)</li>
                            <li>Keeping track of teas we drink and how events go (Records Keeper)</li>
                        </ul>

                        <p>And whatever else, because SocieTea can be whatever you want it to be!</p>

                        <p>This year's CommitTea will show you the ropes (/dishwasher) and help you however necessary to get you started. If you have any questions, you can contact us below...</p>

                        <p>So please consider running for election! Just let us know on the day if you're interested, and you'll need to say a couple of sentences about yourself and why you're suitable for the role.</p>
                        <p>Don't forget, extra curricular activiTEAs look great on your CV :)</p>

                        <h3>CommitTea 2013-14</h3>
                        <p>You can contact us at edinburghSocieTea {at} gmail {dot} com.</p>
                        <ul>
                            <li>El PresidenTea: <a href="http://rhiaro.co.uk">Amy Guy</a> <em>(PhD, Informatics)</em></li>
                            <li>El Vice PresidenTea: Alice Hailwood <em>(4th year, Biology)</em></li>
                            <li>Tearesurer: Albert Lo <em>(Masters, Linguistics)</em></li>
                            <li>SecreTeary: Charlotte Lilley <em>(3rd year, Biology)</em></li>
                            <li>Records Keeper: Alex Kearney <em>(2nd year, Informatics)</em></li>
                            <li>(Extra)Ordinary Member: Matus Falis <em>(2nd year, Informatics)</em></li>
                        </ul>
                        <h3>ConsTeatution 2013-14</h3>
                        <p>Here is our <a href="https://docs.google.com/document/d/1haa-0r6Mkg5OBc8QS9jaxKlKK7y-ms8a3Z0MsuPSg_k/edit?usp=sharing">ConsTeatution</a>, which we'll vote to amend (or not) at the AGM.</p>
                        
                    </div></div>
                    
                </div>
            </div>


            <footer class="w1of1 clearfix dark-bg trans">
                <p></p>
            </footer>

    </body>
</html>
