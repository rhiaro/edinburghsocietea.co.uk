<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>University of Edinburgh SocieTea</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300|Bitter:400,700,400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="../css/normalize.min.css">
        <link rel="stylesheet" href="../css/main.css">

    </head>
    <body class="color2-texture">
        <!--[if lt IE 7]>
            <p class="fail">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

            <div class="wrapper clearfix">
                <header class="w1of4 align-center">
                    <h1><a href="../index.php"><img src="../img/crest.png" alt="The University of Edinburgh SocieTea"></a></h1>
                    <nav>
                        <img src="../img/linetop.png" alt="" role="presentation" />
                        <ul class="inner">
                            <li><a href="http://edinburghsocietea.blogspot.com" class="deco-border">Teablog</a></li>
                        </ul>
                        <img src="../img/linebot.png" alt="" role="presentation" />
                    </nav>
                    
                </header>
                <div class="w3of4">
                    <div class="w1of2"><div class="inner">
                        <h2>Anteaques Tea Tasting</h2>
                        <!-- Embed your google form for signups here... Set width="100%" and height="800"-->
                        <iframe src="https://docs.google.com/forms/d/19bMxAc7k1eFkAaoXR99NvLDkEajDvfv25r5XrYEovLk/viewform?embedded=true" width="100%" height="800" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
                        
                    </div></div>
                    <div class="w1of2"><div class="inner events-holder wee">
                        <? include '../v_events.php'; ?>
                    </div></div>
                </div>
            </div>

            <div class="wrapper clearfix">
                <div class="w1of4"><p></p></div>
                <div class="w3of4"><div class="inner">
                    <? include '../v_sponsors.php'; ?>
                </div></div>
            </div>

            <footer class="w1of1 clearfix dark-bg trans">
                <p></p>
            </footer>

    </body>
</html>
